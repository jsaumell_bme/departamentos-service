package profe.ms.departamentosRest.negocio;

import java.util.List;

import profe.ms.empleados.model.Departamento;

public interface DptosNegocio {

	List<Departamento> getAllDepartamentos();


}
